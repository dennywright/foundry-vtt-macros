if (args[0] === "on") {
(async ()=>{
let casting = Number(args[2]);
let roll =  new Roll(`${casting}d8`).roll();
let optionsText = "";
for (let target of args[1]) {
    if(target.data.disposition === 0){
   optionsText += `<input type="checkbox" id="${target.id}" name="${target.name}" value="${target.id}">${target.name} [${target.actor.data.data.attributes.hp.value}]</option><br>`;
    }
    else {
     return ui.notifications.error(`No valid targets.`);
    }
}
await sleepBox(roll,optionsText);
})();
}

async function sleepBox(roll,optionsText){
let confirmed = false;
let the_content = `<p>Which targets are you putting to sleep?</p>
<form>
<div class="form-group">
<div><p>You have ${roll.total} points total use.
<div name="ability">` + optionsText + `</div>
</div>
</form>
`;
new Dialog({
		title: "Sleep Spell: Choosing Targets",
		content: the_content,
		buttons: {
		go: { label: "Nap Time", callback: () => confirmed = true },
	        cancel: { label: "Cancel", callback: () => confirmed = false }
                },
		close: html => {		     
			if (confirmed) {
			let selected_ability = html.find('[name=ability]')[0].value;
			let empower = actor.items.find(i=> i.name===`${selected_ability}`);
			console.log(empower);
                        let rollMax = new Roll(`${empower.labels.damage}`);
                        let maxDamage = Roll.maximize(`${rollMax.formula}`);
                        console.log(maxDamage);
                        let results_html = `${empower.name} can deal a maximum of ${maxDamage.total}`;
                        ChatMessage.create({
					        user: game.user._id,
                            speaker: ChatMessage.getSpeaker({token: actor}),
                            content: results_html
});
                                        }
                               }
}).render(true);
}