// Item Macro, in DE use @target @item
// Uses Token Magic FX to adds a wave pattern to the token.
let target = canvas.tokens.get(args[1])
let itemD = args[2];
let ActiveEffect = game.macros.getName("ActiveEffect");
let ActorSetFlag = game.macros.getName("ActorSetFlag");
let ActorUnSetFlag = game.macros.getName("ActorUnSetFlag");
let Cub_Condition = game.macros.getName("Cub_Condition");

async function charmON(target) {
let params = [{
    filterType: "wave",
    filterId: "hypnoP",
    enabled: true,
    time: 0,
    anchorX: 0.5,
    anchorY: 0.5,
    strength: 0.015,
    frequency: 120,
    color: 0xFFFFFF,
    maxIntensity: 2.5,
    minIntensity: 0.9,
    padding:10,
    animated :
    {
      time : 
      { 
        active: true, 
        speed: 0.0085, 
        animType: "move" 
      },
      anchorX :
      {
        active: false,
        val1: 0.15,
        val2: 0.85,
        animType: "syncChaoticOscillation",
        loopDuration: 20000
      },
      anchorY :
      {
        active: false,
        val1: 0.15,
        val2: 0.85,
        animType: "syncSinOscillation",
        loopDuration: 20000
      }
    }
}];
await TokenMagic.addUpdateFilters(target, params);
}

async function charmOff(target) {
let params = [{
    filterType: "wave",
    filterId: "hypnoP",
	enabled: false
}];
await TokenMagic.addUpdateFilters(target, params);
}


async function healthCheck(scene, update, flag) {
let crtHP = await update.actorData.flags.world.hypnoP_curtHP_hookID;
let udpHp = await update.actorData.data.attributes.hp.value;
if(crtHP - udpHp > 0) {
let result_html = `<strong>Damage</strong> was dealt to ${target.data.name}, It is no longer under the effect of <strong>${itemD.name}</strong>.`;
    ChatMessage.create({
                            user: game.user._id,
                            speaker: ChatMessage.getSpeaker({token: target}),
                            content: result_html
    });
	gotHurt();
}
}

async function gotHurt() {
Hooks.off("updateToken", healthCheck);
await ActorUnSetFlag.execute(args[1], "world", "hypnoP_hookID");
await ActiveEffect.execute(args[1], itemD.name, "remove");
charmOff(target);
}

if (args[0] === "on") {
const hookId = Hooks.on("updateToken", healthCheck);
const curtHP = target.actor.data.data.attributes.hp.value;
ActorSetFlag.execute(args[1], "world", "hypnoP_hookID", hookId);
ActorSetFlag.execute(args[1], "world", "hypnoP_curtHP_hookID", curtHP);
Cub_Condition.execute(args[1], "incapacitated", "add");
charmON(target);
}

if (args[0] === "off") {
Hooks.off("updateToken", healthCheck);
ActorUnSetFlag.execute(args[1], "world", "hypnoP_hookID");
ActorUnSetFlag.execute(args[1], "world", "hypnoP_curtHP_hookID");
Cub_Condition.execute(args[1], "incapacitated", "remove");
charmOff(target);
}