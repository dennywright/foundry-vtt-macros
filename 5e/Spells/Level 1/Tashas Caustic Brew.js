//###############################################################
// DAE macro
// Duration: Macro repeat on "Start of turn".
// Effects: ItemMacro or Execute, parameters: @actor @item.level
//###############################################################
const lastArg = args[args.length-1];
let actorD = game.actors.get(args[1]._id);
let tokenD = actorD.getActiveTokens()[0];
let target = canvas.tokens.get(lastArg.tokenId);
let itemD = lastArg.efData.flags.dae.itemData;
let level = Number(args[2]);
if(args[0] === "each") {
new Dialog({
  title: `${itemD.name}`,
  content: `<p>Does ${target.name} want to spend an <b>Action</b> to remove ${itemD.name}?</p>`,
  buttons: {
    yes: { label: "Yes", callback: async () => {
        await target.actor.deleteEmbeddedEntity("ActiveEffect", lastArg.effectId);
        let the_message = `${target.name} spends its <b>Acton</b> cleaning off ${itemD.name}`;
        ChatMessage.create({
            user: game.user._id,
            speaker: ChatMessage.getSpeaker({token: target}),
            content: the_message,
            type: CONST.CHAT_MESSAGE_TYPES.EMOTE
        });
        let findEffect = await canvas.tokens.placeables.filter(i => i.actor.effects.find(x=> x.data.label === itemD.name));
        if(findEffect.length === 0){
        let conc = tokenD.actor.effects.find(i=> i.data.label === "Concentrating");
        await tokenD.actor.deleteEmbeddedEntity("ActiveEffect", conc.id);    
        }
    }},
    no: { label: "No", callback: () => {
        let damageRoll = new Roll(`${level*2}d4`).roll();
        new MidiQOL.DamageOnlyWorkflow(actorD, tokenD, damageRoll.total, "acid", [target], damageRoll, {flavor: "(Acid)", itemData: itemD , itemCardId: "new"});
    }}
    },
    default: "Yes",
}).render(true);
}
