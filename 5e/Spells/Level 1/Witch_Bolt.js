//##########################################################
// FIRST READ!!!!!!!!
// Midi-Qol "on use" macro
//##########################################################
if(args[0].hitTargets.length > 0){
  const target = await canvas.tokens.get(args[0].hitTargets[0]._id);
  const level = Number(args[0].spellLevel);
  const damageType = args[0].damageDetail[0].type;
  const itemD = args[0].item;
  const actorD = game.actors.get(args[0].actor._id);
  const tokenD = canvas.tokens.get(args[0].tokenId);

  if(target.inCombat){
    const hookId = Hooks.on("updateCombat", combatRound);
    DAE.setFlag(actorD, "witchBolt", hookId);
  }

  async function witchboltStatus(){
    new Dialog({
      title: `${itemD.name}`,
      content: `<p>Continue concentrating?</p>`,
      buttons: {
        confirmed: {
          label: "Continue",
          callback: () => witchboltDamage()
        },
        cancel: {
          label: "Cancel It!",
          callback: () => witchboltCancel()
        }
      }
    }).render(true);
  }

  async function combatRound(combat, update) {
    if (!("round" in update || "turn" in update)) return;
    if (game.combat.combatant.tokenId === tokenD.id) {
      if(actorD.effects.find(i=> i.data.label === "Concentrating")){
        witchboltStatus();
      } else {
      witchboltCancel();
      }
    }
  }

  async function witchboltDamage(){
    console.log("Witch Bolt Damage");
    let numDie = level;
    let damageRoll = new Roll(`${numDie}d12`).roll();
    game.dice3d?.showForRoll(damageRoll);
    if (itemD.data.components?.concentration ) itemD.data.components.concentration = false;
    new MidiQOL.DamageOnlyWorkflow(actorD, tokenD, damageRoll.total, damageType, [target], damageRoll, {flavor: `(${damageType})`, itemData: itemD , itemCardId: "new"});
  }

  async function witchboltCancel() {
    console.log("Witch Bolt Removal");
    let conc = await actorD.effects.find(i=> i.data.label === "Concentrating");
    if(conc) await actorD.deleteEmbeddedEntity("ActiveEffect", conc.id);
    let hookId = await DAE.getFlag(actorD, "witchBolt", combatRound);
    await Hooks.off("updateCombat", hookId);
    await DAE.unsetFlag(actorD, "witchBolt");
  }
}
