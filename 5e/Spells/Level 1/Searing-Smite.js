// Thanks to both Tposney, Kandashi and Vance for help with this.
// This macro uses Token Magic FX, CUB and DE. To setup in DE set Macro => Item Macro @target @item
// Needs GM Macros to work properly https://gitlab.com/crymic/foundry-vtt-macros/-/tree/master/GM%20Macros
let ActorSetFlag = game.macros.getName("ActorSetFlag");
let ActorUnSetFlag = game.macros.getName("ActorUnSetFlag");
let ActiveEffect = game.macros.getName("ActiveEffect");
let Cub_Condition = game.macros.getName("Cub_Condition");
let target = canvas.tokens.get(args[1]);
let itemD = args[2];
// check for combat rounds
function combatRound(combat, update) {
  if (!("round" in update || "turn" in update)) return;
  if (combat.combatant.tokenId === args[1]) {      
      fireDamage();
  }
}
// token magic fx burning
async function setFire() {
let params =
[{
    filterType: "fire",
    filterId: "searing_Smite",
    autoDestroy: true,
    color: 0xFFFFFF,
    time: 0,
    blend: 2,
    intensity: 1,    
    animated :
    {
      time : 
      { 
        active: true, 
	    loops: 1,
        loopDuration: 1000,
        speed: -0.0024, 
        animType: "move" 
      }
    }
}];
await TokenMagic.addUpdateFilters(target, params);
}
// fake chat card, saving throw and checks if the target is alive or not.
async function fireDamage(){    
    let save_roll = await target.actor.rollAbilitySave('con', {chatMessage : false, fastForward: true });
	let results_html = `<div class="dnd5e chat-card item-card"><header class="card-header flexrow"><img src="${itemD.img}" title="${itemD.name}" width="36" height="36"><h3 class="item-name">${itemD.name}</h3></header><div class="card-content">${itemD.data.description.chat}</div><div class="card-buttons"><div class="flexrow 1"><div style="text-align:center;text-transform:capitalize;">${itemD.data.save.ability} saving throw (DC ${itemD.data.save.dc})<div class="dice-roll"><div class="dice-result"><div class="dice-formula">${save_roll.formula}</div><h4 class="dice-total">${save_roll.total}</h4></div></div></div></div></div><footer class="card-footer"><span>${itemD.data.level} Level</span><span>V</span><span>${itemD.data.activation.cost} ${itemD.data.activation.type} Action</span><span>${itemD.data.target.value} ${itemD.data.target.type}</span><span>${itemD.data.range.value} ${itemD.data.range.units}</span><span>${itemD.data.duration.value} ${itemD.data.duration.units}</span></footer></div>`;
	ChatMessage.create({
					        user: game.user._id,
                            speaker: ChatMessage.getSpeaker({token: target}),
                            content: results_html
	});
	if (target.actor.data.data.attributes.hp.value === 0) {
	 cleanse();	 
	}
	else if(save_roll.total < itemD.data.save.dc) {
	let dmgDie = "1d6";
	let dmgType = "fire";
	let damageRoll = new Roll(`${dmgDie}`).roll();
 	setFire(target);
	new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, dmgType, [target], damageRoll, {flavor: `${itemD.name} - Damage Roll (${dmgType})`});
    } 
    else if (save_roll.total > itemD.data.save.dc) {    
    cleanse();	
    let save_result = `${target.name} successfully made their ${itemD.data.save.ability} saving throw ${save_roll.total} [DC ${itemD.data.save.dc}]`;
    ChatMessage.create({
        user: game.user._id,
        speaker: ChatMessage.getSpeaker({token: target}),
        content: save_result
    });	
    }
}
// clean up effects and DE
function cleanse(){
	Hooks.off("updateCombat", combatRound);
    ActorUnSetFlag.execute(args[1], "world", "searingSmite_hookID");
    ActiveEffect.execute(args[1], "Searing Smite", "remove");	
}
// turn on
if (args[0] === "on") {
    setFire(target);    
    const hookId = Hooks.on("updateCombat", combatRound);
    ActorSetFlag.execute(args[1], "world", "searingSmite_hookID", hookId);
}
// turn off
if (args[0] === "off") {	
	Hooks.off("updateCombat", combatRound);
    Cub_Condition.execute(args[1], "Searing Smite", "remove");
    ActorUnSetFlag.execute(args[1], "world", "searingSmite_hookID");    
}