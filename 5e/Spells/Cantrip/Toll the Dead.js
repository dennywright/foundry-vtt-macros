//Midi-qol on use. Remove damage on item card. Let the macro handle it.
if (args[0].failedSaves.length > 0) {
  let target = canvas.tokens.get(args[0].failedSaves[0]._id);
  let actorD = game.actors.get(args[0].actor._id);
  let tokenD = canvas.tokens.get(args[0].tokenId).actor;
  let level;
  let damageRoll;
  actorD.data.type === "character"
    ? (level = actorD.data.data.details.level)
    : (level = tokenD.data.data.details.cr);
  let numDice = 1 + Math.floor((level + 1) / 6);
  target.actor.data.data.attributes.hp.max !=
  target.actor.data.data.attributes.hp.value
    ? (damageRoll = new Roll(`${numDice}d12`).roll())
    : (damageRoll = new Roll(`${numDice}d8`).roll());
  game.dice3d?.showForRoll(damageRoll);
  new MidiQOL.DamageOnlyWorkflow(
    actorD,
    tokenD,
    damageRoll.total,
    "necrotic",
    [target],
    damageRoll,
    { itemCardId: args[0].itemCardId }
  );

  AudioHelper.play(
    {
      src: "worlds/moonsea-adventures/audio/000_-_toll_the_dead.mp3",
      volume: 0.8,
      autoplay: true,
      loop: false,
    },
    true
  );
}
