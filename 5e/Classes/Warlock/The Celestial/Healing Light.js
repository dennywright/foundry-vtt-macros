//###########################################################################################
// Read this first!!!!!!!!!!!!!!!!!!!!!!!!
// Midi-qol "on use" macro
// Do not add resources or healing to item, let the macro handle everything.
// set ActionType to Utility or Other. Adjust line 12 to resource slot used.
//###########################################################################################
(async()=>{
let target = canvas.tokens.get(args[0].targets[0]._id);
let itemD = args[0].item;
let actorD = game.actors.get(args[0].actor._id);
let tokenD = canvas.tokens.get(args[0].tokenId);
let rollData = await actorD.getRollData();
let resourceSlot = "primary";
let curtRes = rollData.resources[resourceSlot].value;
let maxRes = rollData.resources[resourceSlot].max;
let chrBonus = rollData.abilities.cha.mod;
let finalMax = Math.min(chrBonus, maxRes);
if(curtRes === 0) return ui.notifications.warn(`You are out of the required resources.`);
let content_heal = `<p><em>${itemD.name} on ${target.name} [${curtRes}/${maxRes}] charges left.</em></p><form class="flexcol"><div class="form-group"><label for="num">Dice to Spend:</label><input id="num" name="num" type="number" min="0" max="${finalMax}" placeholder="Current Max ${finalMax}"></input></div></form>`;
new Dialog({
    title: itemD.name,
    content: content_heal,
    buttons: {
        heal: {
            icon: '<i class="fas fa-check"></i>',
            label: 'Heal',
            callback: async (html) => {
                let number = Math.floor(Number(html.find('#num')[0].value));
                if (number < 1 || number > finalMax) return ui.notifications.warn(`Invalid number of charges entered = ${number}. Aborting action.`);
                let healRoll = new Roll(`${number}d6`).roll();
                new MidiQOL.DamageOnlyWorkflow(actorD, tokenD, healRoll.total, "healing", [target], healRoll, {flavor: `(Healing)`, itemCardId: args[0].itemCardId});
                resourceSlot === "primary" ? await actorD.update({"data.resources.primary.value" : curtRes - number}) : resourceSlot === "secondary" ? await actorD.update({"data.resources.secondary.value" : curtRes - number}) : await actorD.update({"data.resources.tertiary.value" : curtRes - number});
            }
        }
    }
}).render(true);
})();
