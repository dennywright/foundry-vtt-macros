// You'll need my Cub GM macro and Kandashi's actor update macro.
// You need to make DAE place a buff on the actor self that's called "Twilight Sanctuary". This macro will detect if it's active or not.
(async ()=>{
let caster = canvas.tokens.controlled[0];
let twilight = await caster.actor.effects.find(ef=> ef.label === "Twilight Sanctuary");
if (!twilight) return game.dnd5e.rollItemMacro("Channel Divinity: Twilight Sanctuary");
let Cub_Condition = game.macros.getName("Cub_Condition");
let ActorUpdate = game.macros.getName("ActorUpdate");
let level = await caster.actor.items.find(i=> i.name === "Cleric").data.data.levels;
let distance = 34.5;
let selectOptions = "";
let targets = canvas.tokens.placeables.filter(target => canvas.grid.measureDistance(caster, target)
< distance && target.data.disposition === caster.data.disposition);
for (let i = 0; i < targets.length; i++){
let target = targets[i];
selectOptions +=`<div class="form-group"><label for="${i}">${target.name}</label><select id="target"><option value="Healing">Healing</option><option value="Charmed">Charmed</option><option value="Frightened">Frightened</option></select></div>`;
}
let the_content = `<p>Pick an ability to use on these targets.</p><form class="flexcol">${selectOptions}</form>`;
new Dialog({
        title: "Twilight Sanctuary",
        content: the_content,
        buttons: {
            yes:{
                icon: '<i class="fas fa-check"></i>',
                label: 'Cure',
                callback: async (html) => {
                for(let i = 0; i < targets.length; i++) {
                let element = await html.find(`select#target`)[i].value;
                let target = targets[i];
                let find_target = await canvas.tokens.placeables.find(t=>t.name===target.name);
                if (element === "Frightened"){
                Cub_Condition.execute(find_target.id, "Frightened", "remove");
                ChatMessage.create({
                        user: game.user._id,
                        speaker: ChatMessage.getSpeaker({token: actor}),
                        content: actor.name + " cures " + target.data.name + " of 1 " + element + " Condition."
                        });

                }
                if (element === "Charmed"){
                Cub_Condition.execute(find_target.id, "Charmed", "remove");
                ChatMessage.create({
                        user: game.user._id,
                        speaker: ChatMessage.getSpeaker({token: actor}),
                        content: actor.name + " cures " + target.data.name + " of 1 " + element + " Condition."
                        });

                }
                if (element === "Healing"){
                let healing = new Roll(`1d6 + ${level}`).roll().total;
                ActorUpdate.execute(find_target.id, ({"data.attributes.hp.temp" : healing}));
                ChatMessage.create({
                        user: game.user._id,
                        speaker: ChatMessage.getSpeaker({token: actor}),
                        content: actor.name + " heals " + target.data.name + " for " + healing  + " points of temp health."
                        });

                }
            }
        }    
      }
    },
    default:"yes"
            
}).render(true);  
})();