// Midi-qol on use, last weapon attack containing (Disadvantage / Critical & Damage Type).
// Asks which weapon used to change damage type.
(async()=>{
  let actorD = game.actors.get(args[0].actor._id);
  let tokenD = canvas.tokens.get(args[0].tokenId);
  let itemD = args[0].item;
  let msgHistory = await Object.values(MidiQOL.DamageOnlyWorkflow.workflows).filter(i=> i.actor.id === actorD.id && i.item?.name != itemD.name);
  if(msgHistory.length === 0) return ui.notifications.warn(`You need to successfully attack first.`);
  let lastAttack = msgHistory[msgHistory.length - 1];
  let target = lastAttack.hitTargets;
  if(lastAttack.disadvantage) return ui.notifications.error(`You currently have disadvantage against this target`);
  let damageType = lastAttack.defaultDamageType;
  let level = actorD.items.getName("Rogue").data.data.levels;
  let numDice = (Math.ceil(level /2));
  let damageRoll = lastAttack.isCritical ? new Roll(`${numDice *2}d6`).roll() : new Roll(`${numDice}d6`).roll();
  game.dice3d?.showForRoll(damageRoll);
  new MidiQOL.DamageOnlyWorkflow(actorD, tokenD, damageRoll.total, damageType, target, damageRoll, {flavor: `(${damageType.charAt(0).toUpperCase() + damageType.slice(1)})`, itemCardId: args[0].itemCardId});
  })();
