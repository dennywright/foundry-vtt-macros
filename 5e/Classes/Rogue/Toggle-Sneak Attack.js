// This macro is straight active effects toggle
let sneakAttack = async function() {
    const effectName = "Sneak Attack";
    const target = canvas.tokens.controlled[0] || game.user.character.getActiveTokens()[0];
    let find_level = target.actor.items.find(i=> i.name === "Rogue");
    if (!find_level) return ui.notifications.error(`No Rogue class found on this character`);
    const level = find_level.data.data.levels;
    const img = "systems/dnd5e/icons/skills/violet_27.jpg";
    let the_message = "";
    if (target.actor.effects.entries.find(ef=> ef.data.label === effectName)) {
        let effect_id = await target.actor.effects.entries.find(ef=> ef.data.label === effectName).id;
        await target.toggleEffect(img, {active : false});
        await target.actor.deleteEmbeddedEntity("ActiveEffect", effect_id);
        the_message = `<em>${effectName} Disabled</em>`;
        } else {
            let effectData = {
                label : effectName,
                icon : img,
                changes: [{
                    "key": "data.bonuses.All-Damage",
                    "mode": 0,
                    "value": `+${(Math.ceil(level /2))}d6`,
                    "priority": 0
                    }]
                }
            await target.actor.createEmbeddedEntity("ActiveEffect", effectData);
            await target.toggleEffect(img, {active:true});
            the_message = `<em>${effectName} Enabled</em>`;
        }
        ChatMessage.create({
            user: game.user._id,
            speaker: ChatMessage.getSpeaker({token: target}),
            content: the_message,
            whisper: ChatMessage.getWhisperRecipients(game.user.name),
            type: CONST.CHAT_MESSAGE_TYPES.EMOTE
        });
    };
sneakAttack();
