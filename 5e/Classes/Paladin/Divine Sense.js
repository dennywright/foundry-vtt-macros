// Midi-qol "On Use", Item Macro. Remove all targeting details, this handles everything.
let get_actor = game.actors.get(args[0].actor._id).getActiveTokens()[0].id;
let paladin = canvas.tokens.get(get_actor);
let distance = 59.5;
let is_target = 0;
let is_good = 0;
let is_evil = 0;
let is_celestial = 0;
let is_fiend = 0;
let is_undead = 0;
function detect(target){
let detect_undead = ["undead"].some(type => (target.actor.data.data.details.type || "").toLowerCase().includes(type));
let detect_celestial = ["celestial"].some(type => (target.actor.data.data.details.type || "").toLowerCase().includes(type));
let detect_fiend = ["fiend"].some(type => (target.actor.data.data.details.type || "").toLowerCase().includes(type));
let detect_good = ["good"].some(alignment => (target.actor.data.data.details.alignment || "").toLowerCase().includes(alignment));
let detect_evil = ["evil"].some(alignment => (target.actor.data.data.details.alignment || "").toLowerCase().includes(alignment));
if(detect_undead) is_undead ++;
if(detect_celestial) is_celestial ++;
if(detect_fiend) is_fiend ++;
if(detect_good) is_good ++;
if(detect_evil) is_evil ++;
}

let get_target = canvas.tokens.placeables.filter(target => (canvas.grid.measureDistance(paladin.center, target.center) <= distance && paladin.id != target.id && !canvas.walls.checkCollision(new Ray(paladin.center, target.center))));
for(let target of get_target){
if(target) is_target ++;
detect(target);
}
let the_message = `<table border="1" style="text-align:center;"><thead><tr><th>Type</th><th>Found</th></tr></thead><tbody><tr><td>Undead</td><td>${is_undead}</td></tr><tr><td>Fiends</td><td>${is_fiend}</td></tr><tr><td>Celestials</td><td>${is_celestial}</td></tr><tr><td>Good Alignment</td><td>${is_good}</td></tr><tr><td>Evil Alignment</td><td>${is_evil}</td></tr></tbody><tbody style="background: rgba(0, 0, 0, 0.5);color: #f0f0e0;text-shadow: 1px 1px #000;border-bottom: 1px solid #000;"><tr><td>Total Sensed</td><td>${is_target}</td></tr></tbody></table>`;
const chatMessage = game.messages.get(args[0].itemCardId);
let content = duplicate(chatMessage.data.content);    
const searchString =  /<div class="midi-qol-saves-display">[\s\S]*<div class="end-midi-qol-saves-display">/g;
const replaceString = `<div class="midi-qol-saves-display"><div class="end-midi-qol-saves-display">${the_message}`;
content = content.replace(searchString, replaceString);
chatMessage.update({content: content});
