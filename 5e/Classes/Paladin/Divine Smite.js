//######################################################################################################
// READ THIS FIRST!!!!!!
// Usage: 
// 1) Midi-qol On Use, best used within Item Macro.
// 2) Create as Spell, Action Type as "Utility" with no damage. The macro will do it for you.
//######################################################################################################

(async()=>{
let choice;
let actorD = game.actors.get(args[0].actor._id);
let tokenD = canvas.tokens.get(args[0].tokenId);
let target = canvas.tokens.get(args[0].targets[0]._id);
let numDice = Number(args[0].spellLevel) + 1;
let undead = ["undead", "fiend"].some(type => (target.actor.data.data.details.type || "").toLowerCase().includes(type));
new Dialog({
  title: "Divine Smite",
  content: "<p>Choose a roll type</p>",
  buttons: {
    roll: { label: "Normal", callback: () => {choice = "norm";}},
    crit: { label: "Critical", callback: () => {choice = "crit";}}
    },
    close: async (html) => {
      if (undead) numDice += 1;
      let damageRoll = choice === "crit" ? new Roll(`${numDice *2}d8`).roll() : new Roll(`${numDice}d8`).roll();
      game.dice3d?.showForRoll(damageRoll);
      new MidiQOL.DamageOnlyWorkflow(actorD, tokenD, damageRoll.total, "radiant", [target], damageRoll, {flavor: "(Radiant)", itemCardId: args[0].itemCardId});
    }
  }
).render(true);
})();
