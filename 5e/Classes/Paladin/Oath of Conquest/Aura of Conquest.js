const get_actor = game.actors.get(args[0].actor._id);
const paladin = canvas.tokens.placeables.find(t => t.name === get_actor.name);
const level = get_actor.getRollData().classes.paladin.levels;
const distance = level >= 18 ? 34.5 : level >= 7 ? 14.5 : 0;
const effect_name = "Channel Divinity: Conquering Presence";
const itemD = args[0].item;
const img = itemD.img;

async function combatRound(combat, update) {
    if (!("round" in update || "turn" in update)) return;
    let get_target = await canvas.tokens.placeables.filter(target => canvas.grid.measureDistance(paladin, target) <= distance && target.actor.effects.find(ef=> ef.data.label === effect_name));
    for(let target of get_target){
        if(combat.combatant.tokenId === target.id){
         let damageRoll = new Roll(`1d${(Math.floor(level/2))}`).roll();
    new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, "psychic", [target], damageRoll, {flavor: `${itemD.name} - Damage Roll`, user: game.user._id,
        speaker: ChatMessage.getSpeaker({token: target})});
        }
    }
}

async function dealDamage(target, level, itemD){
    console.log(`deal damage to ${target.name}`);
    let damageRoll = new Roll(`1d${(Math.floor(level/2))}`).roll();
    new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, "psychic", [target], damageRoll, {flavor: `${itemD.name} - Damage Roll`, user: game.user._id,
        speaker: ChatMessage.getSpeaker({token: target})});
    const effectData = {
        label : itemD.name,
        icon : img,
        changes: [{
            "key": "data.attributes.movement.walk",
            "mode": 5,
            "value": 0,
            "priority": 0
        }]
    };
    ActiveEffect.execute(target.id, effectData, "add");
    game.Gametime.doIn({seconds:6}, async () => {
        ActiveEffect.execute(target.id, itemD.name, "remove");
    });
}

if(game.combat.started){
const hookId = Hooks.on("updateCombat", combatRound);
DAE.setFlag(paladin.actor.id, "auraConquering", hookId);
}
else if ((!game.combat.started) && (DAE.getFlag(paladin.actor.id, "auraConquering")))  {
DAE.unsetFlag(paladin.actor.id, "auraConquering");
}
