// Thanks to Kandashi and Vance
// Requres Kandashi's GM macros and Item Macro, also supports Cub condition. Make sure to apply it on failed save.
// DE => item macro @target @item
let ActorSetFlag = game.macros.getName("ActorSetFlag");
let ActorUnSetFlag = game.macros.getName("ActorUnSetFlag");
let ActorGetFlag = game.macros.getName("ActorSetFlag");
let ActiveEffect = game.macros.getName("ActiveEffect");
let Cub_Condition = game.macros.getName("Cub_Condition");
let actorD = canvas.tokens.find(i => i.name === args[3].name);
let target = canvas.tokens.get(args[1]);
let itemD = args[2];

function combatRound(combat, update) {
  if (!("round" in update || "turn" in update)) return;
  if (combat.combatant.tokenId === args[1]) {
      check_radius();
      game.Gametime.doIn({seconds : 6}, async () => {
      saveRoll();
      });
  }
}

async function check_radius(){
let get_target = await canvas.tokens.placeables.filter(target => (canvas.grid.measureDistance(actorD.center, target.center) <= distance && actorD.id != target.id));
  for(let target of get_target){
  if(target.actor.effects.find(ef=> ef.data.label === itemD.name)){
      let damageDice = Math.floor(ActorD.actor.getRollData().classes.paladin.levels/2);
      let damageRoll = new Roll(`1d${damageDice}`).roll();
      new MidiQOL.DamageOnlyWorkflow(actor, token, damageRoll.total, "psychic", [target], damageRoll, {flavor: `${itemD.name} - Damage Roll (Psychic)`, user: game.user._id, speaker: ChatMessage.getSpeaker({token: target})});
    }
  }
}

async function saveRoll() {
let save_roll = await target.actor.rollAbilitySave('wis', {chatMessage:true, fastForward: true });
if(save_roll.total > itemD.data.save.dc) {
	cleanse();
    let save_result = `${target.name} successfully made their ${itemD.data.save.ability} saving throw ${save_roll.total} [DC ${itemD.data.save.dc}]`;
    ChatMessage.create({
        user: game.user._id,
        speaker: ChatMessage.getSpeaker({token: target}),
        whisper: ChatMessage.getWhisperIDs("GM"),
        content: save_result
    });	
   }
}
 
// Condition Removal
async function cleanse(){	
    await Hooks.off("updateCombat", combatRound);
    await ActorUnSetFlag.execute(args[1], "world", "conqueringPres_hookID");
	await ActiveEffect.execute(args[1], itemD.name, "remove");
}

if (args[0] === "on") {
    Cub_Condition.execute(args[1], "Frightened", "add");
    const hookId = Hooks.on("updateCombat", combatRound);
    ActorSetFlag.execute(args[1], "world", "conqueringPres_hookID", hookId);
}
if (args[0] === "off") {    
	Cub_Condition.execute(args[1], "Frightened", "remove");
}
