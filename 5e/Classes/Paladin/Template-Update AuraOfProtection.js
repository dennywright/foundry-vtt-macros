// later on will turn this into an enable disable trigger
// Requires Active Effect Callback Macro
(async()=>{
async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}
const ActiveEffect = game.macros.getName("ActiveEffect");
const get_actor = game.actors.get(args[0].actor._id);
const find_npc = canvas.tokens.placeables.find(t => t.name === get_actor.name);
const level = await get_actor.getRollData().classes.paladin.levels;
const charisma_mod = get_actor.getRollData().abilities.cha.mod != 0 ? get_actor.getRollData().abilities.cha.mod : 1;
const distance = await level >= 18 ? 34.5 : level >= 7 ? 14.5 : 0;
const itemD = args[0].item;
const effect_name = itemD.name;
const img = itemD.img;
let get_center = find_npc.center;

function aoe_aura(find_npc, get_center, distance){
let get_texture = 'modules/jaamod/AnimatedArt/Runes/teleportationCircle3.webm';
let find_template = canvas.templates.placeables.find(i => i.data.user === game.user.id && i.data.texture === get_texture);
if(!find_template){
    const templateData = [{
    borderColor : "#6b6b6b",
    direction : 40,
    distance : distance,
    fillColor : "#ffffff",
    locked : false,
    t : "circle",
    user : game.user.id,
    x : get_center.x,
    y : get_center.y,
    texture: get_texture
  }];
    MeasuredTemplate.create(templateData);
  }
  else {
    find_template.update({"x": get_center.x, "y": get_center.y});
  }
}


function get_targets(find_npc, target, distance, effect_name, img){
  let get_target = canvas.tokens.placeables.filter(target => (canvas.grid.measureDistance(find_npc.center, target.center) <= distance && find_npc.data.disposition === target.data.disposition));
  for(let target of get_target){
    apply_aura(target, effect_name, img);
  }
}

function remove_targets(find_npc, target, distance, effect_name, img){
  let get_target = canvas.tokens.placeables.filter(target => (canvas.grid.measureDistance(target.center, find_npc.center) > distance && find_npc.id != target.id && find_npc.data.disposition === target.data.disposition));
  for(let target  of get_target){
    remove_aura(target, effect_name, img);
  }
}

function apply_aura(target, effect_name, img){
    
  const effectData = {
    label : effect_name,
    icon : img,
    changes: [{
      "key": "data.bonuses.abilities.save",
      "mode": 2,
      "value": charisma_mod,
      "priority": 0
    }]
  };
  if(!target.actor.effects.find(ef=> ef.data.label === effect_name)){
    ActiveEffect.execute(target.id, effectData, "add");
    target.toggleEffect(img, {active : true});
  }
}

function remove_aura(target, effect_name, img){
  if(target.actor.effects.find(ef=> ef.data.label === effect_name)){
    ActiveEffect.execute(target.id, effect_name, "remove");
    target.toggleEffect(img, {active : false});
  }
}

Hooks.on("updateToken", async (scene, token, update, flags, id) => {
  let target = await canvas.tokens.get(token._id);
  let movement = await getProperty(update, "x") || await getProperty(update, "y");
  if (movement !== undefined) {
    if (target.id === find_npc.id) get_center = find_npc.center;
    aoe_aura(find_npc, get_center);
    await wait(50);
    remove_targets(find_npc, target, distance, effect_name, img);
    await wait(50);
    get_targets(find_npc, target, distance, effect_name, img);
    }
  });
aoe_aura(find_npc, get_center);
})();
