// This will get any defined cursed on an item
// Arguments needed for the macro "item macro or macro execute" @target @item
let target = canvas.tokens.get(args[1])
let itemD = args[2];
let curse = itemD.flags.betterCurses.curseName;
if (args[0] === "on") {
    BetterCurses.curse(curse, args[1]);
}
if (args[0] === "off") {
    BetterCurses.curse(curse, args[1]);
}